# SciHub Addon

#### 简介

【SciHub Addon】超级好用的学术科研插件，完美支持 Sci-Hub、Libgen、unpaywall、OA Button、PubMed、OpenAlex、CORE、ResearchGate、SemanticScholar 等数据库，访问文献网页时，会在文献DOI/PMID/PMCID后面添加 SciHub Addon 图标，点击图标可获取详细文章信息，如IF、分区、引用次数、文章PDF等，点击PDF图标可跳转到下载或文章页，完美支持知网，百度学术，谷歌学术，Scopus，WOS，PubMed 等 400 多个学术期刊网站显示期刊详细等级信息（如JCR/中科院分区、IF/JCI、北大/南大核心、CCF 等 62 个数据库索引情况），支持 40 多个学术网站显示 985、211、双一流、QS 等学校分类排名情况，支持通过 PubPeer、Amend、Retraction Watch 检测学术不端行为，支持文章引用下载功能，支持网页翻译功能，支持部分文献 AI Summary 功能，基于 GPT NLP 技术生成简短的论文摘要。 

[![SciHub Addon](https://pic.rmb.bdstatic.com/bjh/3edce9a334d/240912/9d609fbe1162b0d36df2e42d85953381.png "SciHub Addon")](https://www.scigreat.com/info/sciaddon)


#### 功能

详情请访问：https://www.scigreat.com/info/sciaddon